---

## Voice

简介： 进入设置->时间和语言->语音，配置语音识别，打开cortana进行设置。 之后进入项目，向 Amane Kisora-chan 进行问好（Hello或你好），将会得到回答。

调试难度： 无

基于提供的场景，完成以下tasks：

1. 替换喜欢的模型并完成更多的语音识别应答（Handler） （难度： ★★）
2. 参考Unity官方[STT](https://bitbucket.org/Unity-Technologies/speech-to-text)，替换默认的Windows Speech Recognition Service。（目前新版本IBM Watson由于IBM SDK代码更新且不向下兼容，调试难度较高，不建议尝试） （难度： ★★★★★）
3. 使用[RT-Voice](https://assetstore.unity.com/packages/tools/audio/rt-voice-48394)替换默认Windows TTS服务（如果你会选择这个难度的任务，那么我相信这个插件的免费版对你而言难度也不大了吧） （难度： ★★★★★）

![Voice](https://bitbucket.org/blueprintrealityinc/vr_demo_instruction/raw/master/demo4.png "快来愉快地开始你的第一个VR App吧")

---

Acknowledge

https://assetstore.unity.com/packages/templates/systems/steamvr-plugin-32647

https://bitbucket.org/Unity-Technologies/speech-to-text

https://forum.unity.com/threads/text-to-speech-dll-for-windows-desktop.56038/page-4#post-3340957

https://assetstore.unity.com/packages/3d/characters/amane-kisora-chan-free-ver-70581
