﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySpeechToText.Services;

[RequireComponent(typeof(Animator))]
public class ModelManager : MonoBehaviour
{
    private Animator m_Animator = null;
    private Animator animator
    {
        get
        {
            if(m_Animator == null)
            {
                m_Animator = GetComponent<Animator>();
            }
            return m_Animator;
        }
    }

    private WindowsSpeechToTextService m_SpeechToTextService = null;
    private WindowsSpeechToTextService speechToTextService
    {
        get
        {
            if(m_SpeechToTextService == null)
            {
                m_SpeechToTextService = FindObjectOfType<WindowsSpeechToTextService>();
            }
            return m_SpeechToTextService;
        }
    }

    private VoiceService voice;
    
    void Awake()
    {
        if (speechToTextService != null)
        {
            speechToTextService.RegisterOnError(OnError);
            speechToTextService.RegisterOnTextResult(OnTextResult);
            speechToTextService.RegisterOnRecordingTimeout(OnRecordingTimeout);
        }
    }

    private IEnumerator Start()
    {
        voice = new VoiceService();
        // Wait one frame to make sure stt service is initialized correctly.
        yield return new WaitForEndOfFrame();
        speechToTextService.StartRecording();
    }

    void OnDestroy()
    {
        if (speechToTextService != null)
        {
            speechToTextService.StopRecording();
            speechToTextService.UnregisterOnError(OnError);
            speechToTextService.UnregisterOnTextResult(OnTextResult);
            speechToTextService.UnregisterOnRecordingTimeout(OnRecordingTimeout);
        }
    }
    
    void OnError(string text)
    {
        Debug.LogError(text);
    }

    void OnRecordingTimeout()
    {
        Debug.Log("Timeout");
    }


    // Note that handling interim results is only necessary if your speech-to-text service is streaming.
    // Non-streaming speech-to-text services should only return one result per recording.
    void OnTextResult(SpeechToTextResult result)
    {
        for (int i = 0; i < result.TextAlternatives.Length; ++i)
        {
            Debug.Log("Alternative: (" + i + ")" + result.TextAlternatives[i].Text);
            try
            {
                if (result.TextAlternatives[i].Equals("你好") || result.TextAlternatives[i].Equals("哈喽") || result.TextAlternatives[i].Text.ToLower().Equals("hello"))
                {
                    if(!insideResponse)
                        StartCoroutine("ModelResponse");
                }
            }
            catch { }
        }
    }

    // A block to avoid a second response inject into a previous one
    private bool insideResponse = false;

    IEnumerator ModelResponse()
    {
        insideResponse = true;

        animator.SetBool("param_toidle", false);

        animator.SetBool("param_idletowinpose", true);

        yield return new WaitForSeconds(0.3f);

        speechToTextService.StopRecording();

        if(voice.VoiceInit != 1)
        {
            var textToSay = "<voice required='Name=Microsoft Huihui Desktop'><emph>你好</emph>，我的名字是Amane Kisora-chan。<spell>Amane</spell><silence msec='300'/><spell>Kisorachan</spell>。<silence msec='1000'/>< volume level ='50'>欢迎来到暑期学校，<rate speed='-5'>希望你玩得愉快！";
            voice.SayEX(textToSay, 1 + 8);
        }

        yield return new WaitForSeconds(16f);

        animator.SetBool("param_idletowinpose", false);

        animator.SetBool("param_toidle", true);

        speechToTextService.StartRecording();

        insideResponse = false;
    }
}